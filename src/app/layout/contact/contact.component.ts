import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  public modal: any = {};
  constructor() { }

  ngOnInit() {
  }
  sendMail() {
    if (this.modal.name && this.modal.email && this.modal.enquiry) {
      let body = `Hi,%0d%0a %0d%0a` + `My Name is ` + this.modal.name + ` and i want to enquire about ` + this.modal.enquiry + `and here i'm attaching my contact information.%0d%0a %0d%0a` + this.modal.phone ? `Phone:` + this.modal.phone + `%0d%0a %0d%0a` : `` + `Email:` + this.modal.email + `%0d%0a %0d%0a` + `Thanks`;
      window.location.href = "mailto:?subject=" + this.modal.enquiry + "&body=" + body;
    }
  }
}
