import { NgModule } from '@angular/core';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './components/footer/footer.component';
import { FormsModule } from '@angular/forms';
import { FeaturesComponent } from './features/features.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactComponent } from './contact/contact.component';
import { OurPlansComponent } from './our-plans/our-plans.component';
import { DemoComponent } from './demo/demo.component';
@NgModule({
  declarations: [
    LayoutComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    FeaturesComponent,
    AboutusComponent,
    ContactComponent,
    OurPlansComponent,
    DemoComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    LayoutRoutingModule
  ],
  providers: [
   
  ]
})
export class LayoutModule { }
