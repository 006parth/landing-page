import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactComponent } from './contact/contact.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DemoComponent } from './demo/demo.component';
import { FeaturesComponent } from './features/features.component';
import { LayoutComponent } from './layout.component';
import { OurPlansComponent } from './our-plans/our-plans.component';

const routes: Routes = [
  {
    path: "",
    component: LayoutComponent,
    children: [
      { path: "", redirectTo: "dashboard", pathMatch: "prefix" },
      {
        path: "dashboard",
        component: DashboardComponent
      },
      {
        path: "features",
        component: FeaturesComponent
      },
      {
        path: "about-us",
        component: AboutusComponent
      },
      {
        path: "contact-us",
        component: ContactComponent
      },
      {
        path: "plans",
        component: OurPlansComponent
      },
      {
        path: "demo",
        component: DemoComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
